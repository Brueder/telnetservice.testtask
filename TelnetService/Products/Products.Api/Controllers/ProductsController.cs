﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using TelnetService.Infrastructure.Filters;
using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models;
using TelnetService.Infrastructure.Models.Product;

namespace Products.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ContextAuthorizationFilter]
    public class ProductsController : ControllerBase
    {
        private readonly IBusControl _busControl;
        private readonly Uri _rabbitMqUrl = new Uri("rabbitmq://localhost/productsQueue");

        public ProductsController(IBusControl busControl)
        {
            _busControl = busControl;
        }

        [HttpGet("getarea")]
        public async Task<IActionResult> GetArea([FromQuery]int area)
        {
            var response = await GetResponseRabbitTask<AreaRequest, ProductsResponse>(new AreaRequest(area));
            return Ok(ApiResult<ICollection<Product>>.Success200(response.Products));
        }

        [HttpGet("getbyid")]
        public async Task<IActionResult> GetById([FromQuery] int id)
        {
            var response = await GetResponseRabbitTask<IndexRequestResponse<int>, ProductsResponse>(new IndexRequestResponse<int>(id));
            return Ok(ApiResult<ICollection<Product>>.Success200(response.Products));
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreateProductRequest product)
        {
            var response = await GetResponseRabbitTask<CreateProductRequest, IndexRequestResponse<int>>(product);
            return Ok(ApiResult<int>.Success200(response.Id));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromQuery]int id, [FromBody] CreateProductRequest createProductRequest)
        {
            var request = new UpdateProductRequest()
            {
                Id = id,
                Product = new Product()
                {
                    Price = createProductRequest.Price,
                    Name = createProductRequest.Name,
                    Description = createProductRequest.Description,
                }
            };

            var response = await GetResponseRabbitTask<UpdateProductRequest, IndexRequestResponse<int>>(request);
            return Ok(ApiResult<int>.Success200(request.Id));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] int id)
        {
            var response = await GetResponseRabbitTask<IndexRequestResponse<int>, IndexRequestResponse<int>>(new IndexRequestResponse<int>(id));
            return Ok(ApiResult<int>.Success200(response.Id));
        }

        private async Task<TOut> GetResponseRabbitTask<TIn, TOut>(TIn request) where TIn : class where TOut : class
        {
            var client = _busControl.CreateRequestClient<TIn>(_rabbitMqUrl);
            var response = await client.GetResponse<TOut>(request);
            return response.Message;
        }
    }
}
