﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;

namespace Products.Api.Consumers
{
    /// <summary>
    /// Позволяет слушать запросы по возвращению продуктов по области
    /// </summary>
    public class GetAreaProductsConsumer : IConsumer<AreaRequest>
    {
        private readonly IProductService _productService;
        private readonly ILogger<GetAreaProductsConsumer> _logger;

        public GetAreaProductsConsumer(IProductService productService, ILogger<GetAreaProductsConsumer> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<AreaRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(GetAreaProductsConsumer)} | data: {context.Message.Area}");
            var order = _productService.GetAreaProducts(context.Message.Area);
            await context.RespondAsync(new ProductsResponse(order));
        }
    }
}
