﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Requests;

namespace Products.Api.Consumers
{
    /// <summary>
    /// Позволяет слушать запросы по обновлению продукта
    /// </summary>
    public class UpdateProductConsumer : IConsumer<UpdateProductRequest>
    {
        private readonly IProductService _productsService;
        private readonly ILogger<UpdateProductConsumer> _logger;

        public UpdateProductConsumer(IProductService productsService, ILogger<UpdateProductConsumer> logger)
        {
            _productsService = productsService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UpdateProductRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(UpdateProductConsumer)} | data: id {context.Message.Id} name {context.Message.Product.Name} price {context.Message.Product.Price}");
            var order = await _productsService.UpdateProduct(context.Message);
            await context.RespondAsync(order);
        }
    }
}
