﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models.Product;

namespace Products.Api.Consumers
{
    /// <summary>
    /// Позволяет слушать запросы по возвращению продукта по его id
    /// </summary>
    public class GetByIdProductConsumer : IConsumer<IndexRequestResponse<int>>
    {
        private readonly IProductService _productService;
        private readonly ILogger<GetByIdProductConsumer> _logger;

        public GetByIdProductConsumer(IProductService productService, ILogger<GetByIdProductConsumer> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IndexRequestResponse<int>> context)
        {
            _logger.LogInformation($"Consume: {nameof(GetProductsByIdsConsumer)} | data: {context.Message.Id}");
            var order = await _productService.GetProductById(context.Message.Id);
            await context.RespondAsync(new ProductsResponse(new List<Product> { order })); 
        }
    }
}
