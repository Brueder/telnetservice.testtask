﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Common;

namespace Products.Api.Consumers
{
    public class DeleteProductConsumer : IConsumer<IndexRequestResponse<int>>
    {
        private readonly IProductService _productsService;
        private readonly ILogger<DeleteProductConsumer> _logger;

        public DeleteProductConsumer(IProductService productsService, ILogger<DeleteProductConsumer> logger)
        {
            _productsService = productsService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IndexRequestResponse<int>> context)
        {
            var order = await _productsService.DeleteProduct(context.Message.Id);
            await context.RespondAsync(order);
        }
    }
}
