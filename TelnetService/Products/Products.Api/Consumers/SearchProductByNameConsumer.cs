﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Requests;

namespace Products.Api.Consumers
{
    /// <summary>
    /// Позволяет слушать запросы по поиску продуктов по их имени
    /// </summary>
    public class SearchProductByNameConsumer : IConsumer<SearchByNameRequest>
    {
        private readonly IProductService _productService;
        private readonly ILogger<SearchProductByNameConsumer> _logger;

        public SearchProductByNameConsumer(IProductService productService, ILogger<SearchProductByNameConsumer> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<SearchByNameRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(SearchProductByNameConsumer)} | data: {context.Message.Name}");
            var products = _productService.SearchProductsByName(context.Message.Name);
            await context.RespondAsync(products);
        }
    }
}
