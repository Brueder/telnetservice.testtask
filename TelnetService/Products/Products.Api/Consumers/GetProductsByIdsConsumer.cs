﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Common;

namespace Products.Api.Consumers
{
    public class GetProductsByIdsConsumer : IConsumer<IndexRequestResponse<List<int>>>
    {
        private IProductService _productService;
        private ILogger<GetProductsByIdsConsumer> _logger;

        public GetProductsByIdsConsumer(IProductService productService, ILogger<GetProductsByIdsConsumer> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IndexRequestResponse<List<int>>> context)
        {
            var order = _productService.GetProductsByIds(context.Message.Id);
            await context.RespondAsync(order);
        }
    }
}
