﻿using MassTransit;
using Products.Core.Services;
using TelnetService.Infrastructure.MassTransit.Requests;

namespace Products.Api.Consumers
{
    /// <summary>
    /// Позволяет слушать запросы по созданию продукта
    /// </summary>
    public class CreateProductConsumer : IConsumer<CreateProductRequest>
    {
        private readonly IProductService _productService;
        private readonly ILogger<CreateProductConsumer> _logger;

        public CreateProductConsumer(IProductService productService, ILogger<CreateProductConsumer> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<CreateProductRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(CreateProductConsumer)} | data: name {context.Message.Name} price {context.Message.Price} decription {context.Message.Description}");
            var order = await _productService.AddProduct(context.Message);
            await context.RespondAsync(order);
        }
    }
}
