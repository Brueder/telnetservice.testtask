using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Products.Api.Definitions;
using Products.Core.Database;
using Products.Core.Services;
using System.Text;
using TelnetService.Infrastructure.Middleware;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.UseUrls("http://*:8002");

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDefinitions(builder, typeof(Program));

builder.Services.AddDbContext<ProductsDbContext>(
    option => option.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);

builder.Services.AddScoped<IProductService, ProductsService>();

var app = builder.Build();

//app.UseHttpsRedirection();
app.UseRouting();

app.UseMiddleware<JwtMiddleware>();

app.MapControllers();

app.UseDefinitions();

app.Run();
