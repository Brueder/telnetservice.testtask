﻿using MassTransit;
using Products.Api.Consumers;

namespace Products.Api.Definitions.MassTransit
{
    public class MassTransitDefinition : Definition
    {
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.AddConsumer<CreateProductConsumer>();
                x.AddConsumer<GetAreaProductsConsumer>();
                x.AddConsumer<GetByIdProductConsumer>();
                x.AddConsumer<UpdateProductConsumer>();
                x.AddConsumer<DeleteProductConsumer>();
                x.AddConsumer<SearchProductByNameConsumer>();
                x.AddConsumer<GetProductsByIdsConsumer>();
                x.UsingRabbitMq((context, cfg) =>
                {

                    cfg.Host(new Uri("rabbitmq://host.docker.internal/"));
                    cfg.ReceiveEndpoint("productsQueue", e =>
                    {
                        e.PrefetchCount = 20;
                        e.UseMessageRetry(r => r.Interval(2, 100));

                        e.Consumer<GetAreaProductsConsumer>(context);
                        e.Consumer<CreateProductConsumer>(context);
                        e.Consumer<GetByIdProductConsumer>(context);
                        e.Consumer<UpdateProductConsumer>(context);
                        e.Consumer<DeleteProductConsumer>(context);
                        e.Consumer<SearchProductByNameConsumer>(context);
                        e.Consumer<GetProductsByIdsConsumer>(context);
                    });
                });

            });

            services.AddMassTransitHostedService();
        }
    }
}
