﻿namespace Products.Api.Definitions
{
    public static class DefinitionExtensions
    {
        public static void AddDefinitions(this IServiceCollection services, WebApplicationBuilder builder, params Type[] entryPointsAssembly)
        {
            var definitions = new List<IDefinition>();

            foreach (var entryPoint in entryPointsAssembly)
            {
                //Вытаскивает все типы реализуещие IDefinition
                var types = entryPoint.Assembly.ExportedTypes.Where(t => !t.IsAbstract && typeof(IDefinition).IsAssignableFrom(t));  
                //Создаём объекты извесных типов и записываем 
                var instances = types.Select(Activator.CreateInstance).Cast<IDefinition>();
                definitions.AddRange(instances);
            }

            // Выполнение всех конфигураций
            definitions.ForEach(definition => definition.ConfigureService(services, builder.Configuration));
            // Добавление всех difinition как колекцию для чтение, чтобы метод ниже её вытащил
            services.AddSingleton(definitions as IReadOnlyCollection<IDefinition>);
        }

        public static void UseDefinitions(this WebApplication application)
        {
            var definitions = application.Services.GetRequiredService<IReadOnlyCollection<IDefinition>>();
            var environment = application.Services.GetRequiredService<IWebHostEnvironment>();
            foreach(var endpoint in definitions)
            {
                endpoint.ConfigureApplication(application, environment);
            }
        }
    }
}
