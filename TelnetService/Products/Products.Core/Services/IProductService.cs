﻿using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models.Product;

namespace Products.Core.Services
{
    public interface IProductService
    {
        public Task<Product> GetProductById(int id);
        public ICollection<Product> GetAreaProducts(int area);
        public Task<IndexRequestResponse<int>> AddProduct(CreateProductRequest product);
        public Task<IndexRequestResponse<int>> UpdateProduct(UpdateProductRequest product);
        public Task<IndexRequestResponse<int>> DeleteProduct(int id);
        public ProductsResponse SearchProductsByName(string name);
        public ProductsResponse GetProductsByIds(IEnumerable<int> ids);
    }
}
