﻿using Products.Core.Database;
using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models.Product;

namespace Products.Core.Services
{
    public class ProductsService : IProductService
    {
        private readonly ProductsDbContext _productsDbContext;

        public ProductsService(ProductsDbContext productsDbContext)
        {
            _productsDbContext = productsDbContext;
        }

        public async Task<IndexRequestResponse<int>> AddProduct(CreateProductRequest product)
        {
            var model = new Product
            {
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
            };

            await _productsDbContext.Products.AddAsync(model);
            await _productsDbContext.SaveChangesAsync();
            return new IndexRequestResponse<int>(model.Id);
        }

        public async Task<IndexRequestResponse<int>> DeleteProduct(int id)
        {
            var product = _productsDbContext.Products.Where(x => x.Id == id).FirstOrDefault();
            if (product is null)
            {
                throw new ArgumentException($"product id {id} not foind");
            }

            _productsDbContext.Products.Remove(product);
            await _productsDbContext.SaveChangesAsync();
            return new IndexRequestResponse<int>(id);
        }

        public ICollection<Product> GetAreaProducts(int area)
        {
            return _productsDbContext.Products.Take(area).ToList();
        }

        public async Task<Product> GetProductById(int id)
        {
            var product = await _productsDbContext.Products.FindAsync(id);
            if(product is null)
            {
                throw new ArgumentException($"Product id {id} not found");
            }

            return product;
        }

        public ProductsResponse GetProductsByIds(IEnumerable<int> ids)
        {
            var products = _productsDbContext.Products.Where(p => ids.Contains(p.Id));
            return new ProductsResponse(products.ToList());
        }

        public ProductsResponse SearchProductsByName(string name)
        {
            var products = _productsDbContext.Products
                .Where(x =>
                x.Name.ToLower()
                      .Contains(name.ToLower()))
                .ToList();

            return new ProductsResponse(products ?? new List<Product>());
        }

        public async Task<IndexRequestResponse<int>> UpdateProduct(UpdateProductRequest request)
        {
            var product = _productsDbContext.Products.Where(x => x.Id == request.Id).FirstOrDefault();
            if(product is null)
            {
                throw new ArgumentException($"product id {request.Id} not foind");
            }

            product.Name = String.IsNullOrEmpty(request.Product.Name) ? product.Name : request.Product.Name;
            product.Price = request.Product.Price > 0 ? request.Product.Price : product.Price;
            product.Description = String.IsNullOrEmpty(request.Product.Description) ? product.Description : request.Product.Description;

            _productsDbContext.Update(product);
            await _productsDbContext.SaveChangesAsync();

            return new IndexRequestResponse<int>(request.Id);
        }
    }
}
