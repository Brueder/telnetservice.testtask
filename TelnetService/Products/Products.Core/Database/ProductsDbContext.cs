﻿using Microsoft.EntityFrameworkCore;
using TelnetService.Infrastructure.Models.Product;

namespace Products.Core.Database
{
    public class ProductsDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; } = null!;
        public ProductsDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
