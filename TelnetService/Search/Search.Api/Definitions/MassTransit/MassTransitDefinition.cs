﻿using MassTransit;

namespace Search.Api.Definitions.MassTransit
{
    public class MassTransitDefinition : Definition
    {
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(new Uri("rabbitmq://host.docker.internal/"));
                });

            });

            services.AddMassTransitHostedService();
        }
    }
}
