﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using TelnetService.Infrastructure.Filters;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models;
using TelnetService.Infrastructure.Models.Indentity;
using TelnetService.Infrastructure.Models.Product;

namespace Search.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ContextAuthorizationFilter]
    public class SearchController : ControllerBase
    {
        private readonly IBusControl _busControl;
        private readonly ILogger<SearchController> _logger;
        private readonly IRequestClient<SearchByNameRequest> _requestClient;
        private readonly Uri _rabbitMqProductsUrl = new Uri("rabbitmq://localhost/productsQueue");
        private readonly Uri _rabbitMqUsersUrl = new Uri("rabbitmq://localhost/identityQueue");

        public SearchController(IBusControl busControl, IRequestClient<SearchByNameRequest>  requestClient, ILogger<SearchController> logger)
        {
            _busControl = busControl;
            _requestClient = requestClient;
            _logger = logger;
        }

        [HttpGet("productsbyname")]
        public async Task<IActionResult> SearchProductsByName([FromQuery]string name)
        {
            var productsResponse = await GetResponseRabbitTask<SearchByNameRequest, ProductsResponse>(new SearchByNameRequest(name), _rabbitMqProductsUrl);
            return Ok(ApiResult<ICollection<Product>>.Success200(productsResponse.Products));
        }

        [HttpGet("usersbyname")]
        public async Task<IActionResult> SearchUsersByName([FromQuery]string name)
        {
            var users = await GetResponseRabbitTask<SearchByNameRequest, UsersResponse>(new SearchByNameRequest(name), _rabbitMqUsersUrl);
            return Ok(ApiResult<ICollection<ApplicationUser>>.Success200(users.Users));
        }

        private async Task<TOut> GetResponseRabbitTask<TIn, TOut>(TIn request, Uri uri) where TIn : class where TOut : class
        {
            var client = _busControl.CreateRequestClient<TIn>(uri);
            var response = await client.GetResponse<TOut>(request);
            return response.Message;
        }
    }
}
