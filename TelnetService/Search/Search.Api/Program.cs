using Search.Api.Definitions;
using TelnetService.Infrastructure.Middleware;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.UseUrls("http://*:8003");
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDefinitions(builder, typeof(Program));

var app = builder.Build();
app.UseDefinitions();
app.UseMiddleware<JwtMiddleware>();
app.MapControllers();
app.Run();
