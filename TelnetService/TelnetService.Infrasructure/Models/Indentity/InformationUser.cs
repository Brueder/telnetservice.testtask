﻿namespace TelnetService.Infrastructure.Models.Indentity
{
    public class InformationUser
    {
        public string Id { get; set; }
        public string Username { get; set; }
    }
}
