﻿using System.ComponentModel.DataAnnotations;

namespace TelnetService.Infrastructure.Models.Indentity
{
    /// <summary>
    /// Запрос аунтификации
    /// </summary>
    public class AuthenticateRequest
    {
        [Required]
        [MaxLength(30)]
        public string Username { get; set; }
        [Required]
        [MaxLength(64)]
        public string Password { get; set; }
    }
}
