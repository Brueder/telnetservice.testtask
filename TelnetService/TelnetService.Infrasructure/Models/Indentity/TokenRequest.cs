﻿namespace TelnetService.Infrastructure.Models.Indentity
{
    /// <summary>
    /// Токен, который выдаётся в ответ на запрос
    /// </summary>
    public class TokenRequest
    {
        public string Token { get; set; }
    }
}
