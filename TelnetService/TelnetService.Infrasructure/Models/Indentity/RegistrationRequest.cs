﻿using System.ComponentModel.DataAnnotations;

namespace TelnetService.Infrastructure.Models.Indentity
{
    /// <summary>
    /// Запрос регитсрации
    /// </summary>
    public class RegistrationRequest
    {
        [Required]
        [MaxLength(30)]
        public string Username { get; set; } = null!;
        
        [Required]
        [MaxLength(64)]
        public string Password { get; set; } = null!;
        
        [MaxLength(64)]
        public string? Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"((\+7|8|\b)[\(\s-]*(\d)[\s-]*(\d)[\s-]*(\d)[)\s-]*(\d)[\s-]*(\d)[\s-]*(\d)[\s-]*(\d)[\s-]*(\d)[\s-]*(\d)[\s-]*(\d))")]
        [MaxLength(20)]
        public string? PhoneNumber { get; set; }
    }
}
