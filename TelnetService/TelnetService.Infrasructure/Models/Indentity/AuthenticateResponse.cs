﻿namespace TelnetService.Infrastructure.Models.Indentity
{
    /// <summary>
    /// Ответ антификации
    /// </summary>
    public class AuthenticateResponse
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }
        public AuthenticateResponse(InformationUser user, string token)
        {
            Id = user.Id;
            UserName = user.Username;
            Token = token;
        }

        public AuthenticateResponse() { }
    }
}
