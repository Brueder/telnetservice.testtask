﻿using System.ComponentModel.DataAnnotations;

namespace TelnetService.Infrastructure.Models.Indentity
{
    public class Order
    {
        [Required]
        [Key]
        public int Id { get; set; }
        
        public DateTime DateTime { get; set; }
        
        [Required]
        public ApplicationUser User { get; set; } = null!;

        [Required]
        public virtual ICollection<OrderProducts> ProductIds { get; set; } = null!;
        
    }

    public class OrderProducts
    {
        [Required]
        [Key]
        public int OrderProductsId { get; set; }

        [Required]
        public int OrderId { get; set; }

        [Required]
        public int ProductId { get; set; }
        public Order Order { get; set; }
        
    }
}
