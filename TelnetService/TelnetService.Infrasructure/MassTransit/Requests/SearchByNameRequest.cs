﻿namespace TelnetService.Infrastructure.MassTransit.Requests
{
    public class SearchByNameRequest
    {
        public SearchByNameRequest(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}
