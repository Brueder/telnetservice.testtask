﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelnetService.Infrastructure.Models.Product;

namespace TelnetService.Infrastructure.MassTransit.Requests
{
    public class UpdateProductRequest
    {
        public int Id { get; set; }
        public Product Product { get; set; } = null!;
    }
}
