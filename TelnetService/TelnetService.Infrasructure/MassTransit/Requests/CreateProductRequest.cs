﻿using System.ComponentModel.DataAnnotations;

namespace TelnetService.Infrastructure.MassTransit.Requests
{
    public class CreateProductRequest
    {
        [Required]
        public string Name { get; set; } = null!;
        [Required]
        public int Price { get; set; }
        public string? Description { get; set; }
    }
}
