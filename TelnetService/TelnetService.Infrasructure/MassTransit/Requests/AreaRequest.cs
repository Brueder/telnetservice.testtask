﻿namespace TelnetService.Infrastructure.MassTransit.Requests
{
    public class AreaRequest
    {
        public AreaRequest(int area)
        {
            Area = area;
        }
        public int Area { get; set; }
    }
}
