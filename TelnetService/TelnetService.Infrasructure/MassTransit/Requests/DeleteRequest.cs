﻿namespace TelnetService.Infrastructure.MassTransit.Requests
{
    public class DeleteRequest<T>
    {
        public DeleteRequest(T id)
        {
            Id = id;
        }
        public T Id { get; set; }
    }
}
