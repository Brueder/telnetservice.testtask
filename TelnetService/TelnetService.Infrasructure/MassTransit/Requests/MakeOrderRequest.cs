﻿using System.ComponentModel.DataAnnotations;

namespace TelnetService.Infrastructure.MassTransit.Requests
{
    public class MakeOrderRequest
    {
        [Required]
        public string UserId { get; set; } = null!;
        [Required]
        public List<int> ProductIds { get; set; } = null!;
    }
}
