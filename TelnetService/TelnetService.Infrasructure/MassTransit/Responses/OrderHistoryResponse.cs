﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelnetService.Infrastructure.MassTransit.Responses
{
    public class OrderHistoryResponse
    {
        public List<OrderResponse> OrderResponses { get; set; } = null!;
        public int Price { get; set; }
        public int ProductCount { get; set; }
    }
}
