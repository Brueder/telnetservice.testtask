﻿using TelnetService.Infrastructure.Models.Product;

namespace TelnetService.Infrastructure.MassTransit.Responses
{
    public class OrderResponse
    {
        public int Id { get; set; }

        public DateTime DateTime { get; set; }

        public virtual List<Product> Products { get; set; } = null!;
    }
}
