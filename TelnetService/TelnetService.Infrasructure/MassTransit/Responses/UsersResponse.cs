﻿using TelnetService.Infrastructure.Models.Indentity;

namespace TelnetService.Infrastructure.MassTransit.Responses
{
    public class UsersResponse
    {
        public UsersResponse(List<ApplicationUser> users)
        {
            Users = users;
        }

        public List<ApplicationUser> Users { get; set; } = null!;
    }
}
