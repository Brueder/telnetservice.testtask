﻿using TelnetService.Infrastructure.Models.Product;

namespace TelnetService.Infrastructure.MassTransit.Responses
{
    public class ProductsResponse
    {
        public ProductsResponse(ICollection<Product> products)
        {
            Products = products;
        }
        public ICollection<Product> Products { get; set; }
    }
}
