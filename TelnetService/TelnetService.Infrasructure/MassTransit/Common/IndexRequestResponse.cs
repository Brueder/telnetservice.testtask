﻿namespace TelnetService.Infrastructure.MassTransit.Common
{
    public class IndexRequestResponse<T>
    {
        public IndexRequestResponse(T id)
        {
            Id = id;
        }

        public T Id { get; set; }
    }
}
