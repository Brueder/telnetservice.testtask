﻿using Microsoft.AspNetCore.Mvc.Filters;
using TelnetService.Infrastructure.Models.Indentity;

namespace TelnetService.Infrastructure.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ContextAuthorizationFilter : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!(context.HttpContext.Items["User"] is InformationUser))
                throw new Exception("User unauthorized");
        }
    }
}
