using Identity.Core.Services;
using Indentity.Api.Definitions;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.UseUrls("http://*:8001");

// Add services to the container.
builder.Services.AddDefinitions(builder, typeof(Program));
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IOrderService, OrderService>();
//builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();

app.UseDefinitions();;

app.Run();
