﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using TelnetService.Infrastructure.Filters;
using TelnetService.Infrastructure.MassTransit;
using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models;

namespace Identity.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ContextAuthorizationFilter]
    public class OrderController : ControllerBase
    {
        private readonly IBusControl _busControl;
        private readonly ILogger<OrderController> _logger;
        private readonly Uri _rabbitMqUrl = new Uri("rabbitmq://localhost/orderQueue");

        public OrderController(IBusControl busControl, ILogger<OrderController> logger)
        {
            _busControl = busControl;
            _logger = logger;
        }

        [HttpGet("getbyid")]
        public async Task<IActionResult> GetOrderById([FromQuery] int orderid)
        {
            var response = await GetResponseRabbitTask<IndexRequestResponse<int>, OrderResponse>(new IndexRequestResponse<int>(orderid));
            return Ok(ApiResult<OrderResponse>.Success200(response));
        }

        [HttpGet("getuserhistory")]
        public async Task<IActionResult> GetOrdersByUserId([FromQuery] string userid)
        {
            var response = await GetResponseRabbitTask<IndexRequestResponse<string>, OrderHistoryResponse>(new IndexRequestResponse<string>(userid));
            return Ok(ApiResult<OrderHistoryResponse>.Success200(response));
        }

        [HttpPost("make")]
        public async Task<IActionResult> MakeOrder([FromBody]MakeOrderRequest request)
        {
            var response = await GetResponseRabbitTask<MakeOrderRequest, IndexRequestResponse<int>>(request);
            return Ok(ApiResult<IndexRequestResponse<int>>.Success200(response));
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteOrder([FromQuery] int orderid)
        {
            var response = await GetResponseRabbitTask<DeleteRequest<int>, StandartResponse>(new DeleteRequest<int>(orderid));
            return Ok(ApiResult<StandartResponse>.Success200(response));
        }

        private async Task<TOut> GetResponseRabbitTask<TIn, TOut>(TIn request) where TIn : class where TOut : class
        {
            var client = _busControl.CreateRequestClient<TIn>(_rabbitMqUrl);
            var response = await client.GetResponse<TOut>(request);
            return response.Message;
        }
    }
}
