﻿using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TelnetService.Infrastructure.Filters;
using TelnetService.Infrastructure.MassTransit;
using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models;
using TelnetService.Infrastructure.Models.Indentity;

namespace Indentity.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IBusControl _busControl;
        private readonly Uri _rabbitMqUrl = new Uri("rabbitmq://localhost/identityQueue");

        public UserController(IBusControl busControl)
        {
            _busControl = busControl;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] AuthenticateRequest model)
        {
            var response = await GetResponseRabbitTask<AuthenticateRequest, AuthenticateResponse>(model);
            return Ok(ApiResult<AuthenticateResponse>.Success200(response));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationRequest model)
        {
            var response = await GetResponseRabbitTask<RegistrationRequest, IdentityResult>(model);
            return Ok(ApiResult<IdentityResult>.Success200(response));
        }

        [HttpGet("currentinformationuser")]
        [ContextAuthorizationFilter]
        public async Task<IActionResult> GetCurrentInformationUser()
        {
            var user = HttpContext.Items["User"] as InformationUser;
            return Ok(ApiResult<InformationUser>.Success200(user));
        }

        [HttpGet("getbyid")]
        [ContextAuthorizationFilter]
        public async Task<IActionResult> GetUserById([FromQuery] string id)
        {
            var response = await GetResponseRabbitTask<IndexRequestResponse<string>, ApplicationUser>(new IndexRequestResponse<string>(id));
            return Ok(ApiResult<ApplicationUser>.Success200(response));
        }

        [ContextAuthorizationFilter]
        [HttpDelete]
        public async Task<IActionResult> DeleteUser([FromQuery] string id)
        {
            var response = await GetResponseRabbitTask<DeleteRequest<string>, StandartResponse>(new DeleteRequest<string>(id));
            return Ok(ApiResult<StandartResponse>.Success200(response));
        }

        /// <summary>
        /// Помогает забрать ответ из RabbitMQ
        /// </summary>
        /// <typeparam name="TIn"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task<TOut> GetResponseRabbitTask<TIn, TOut>(TIn request)
        where TIn : class
        where TOut : class
        {
            var client = _busControl.CreateRequestClient<TIn>(_rabbitMqUrl);
            var response = await client.GetResponse<TOut>(request);
            return response.Message;
        }
    }
}
