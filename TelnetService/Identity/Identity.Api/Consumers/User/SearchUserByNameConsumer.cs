﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Requests;

namespace Identity.Api.Consumers.User
{
    /// <summary>
    /// Посзволяет слушать запросы по поиску пользователя по его имени
    /// </summary>
    public class SearchUserByNameConsumer : IConsumer<SearchByNameRequest>
    {
        private readonly IUserService _userService;
        private readonly ILogger<SearchUserByNameConsumer> _logger;

        public SearchUserByNameConsumer(IUserService userService, ILogger<SearchUserByNameConsumer> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<SearchByNameRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(SearchByNameRequest)} | data: {context.Message.Name}");
            var users = _userService.SearchUsersByName(context.Message.Name);
            await context.RespondAsync(users);
        }
    }
}
