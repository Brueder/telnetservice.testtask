﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.Models.Indentity;

namespace Identity.Api.Consumers.User
{
    /// <summary>
    /// Позволяет слушать запросы по созданию нового пользователя
    /// </summary>
    public class CreateUserConsumer : IConsumer<RegistrationRequest>
    {
        private readonly IUserService _userService;
        private readonly ILogger<CreateUserConsumer> _logger;

        public CreateUserConsumer(IUserService userService, ILogger<CreateUserConsumer> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<RegistrationRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(CreateUserConsumer)} | data: username {context.Message.Username} password {context.Message.Password}");
            var order = await _userService.CreateUser(context.Message);
            await context.RespondAsync(order);
        }
    }
}
