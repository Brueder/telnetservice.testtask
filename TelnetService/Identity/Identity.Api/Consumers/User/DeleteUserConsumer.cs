﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;

namespace Identity.Api.Consumers.User
{
    /// <summary>
    /// Позволяет слущать запросты по удалению пользователей
    /// </summary>
    public class DeleteUserConsumer : IConsumer<DeleteRequest<string>>
    {
        private readonly IUserService _userService;
        private readonly ILogger<DeleteUserConsumer> _logger;

        public DeleteUserConsumer(IUserService userService, ILogger<DeleteUserConsumer> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<DeleteRequest<string>> context)
        {
            _logger.LogInformation($"Consume: {nameof(DeleteUserConsumer)} | data: {context.Message.Id}");
            await _userService.DeleteUser(context.Message.Id);
            await context.RespondAsync(new StandartResponse());
        }
    }
}
