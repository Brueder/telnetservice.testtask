﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.Models.Indentity;

namespace Identity.Api.Consumers.User
{
    /// <summary>
    /// Позволяет слушать запросы по аунтификации пользователя
    /// </summary>
    public class AuthenticateConsumer : IConsumer<AuthenticateRequest>
    {
        private readonly IUserService _userService;
        private readonly ILogger<AuthenticateConsumer> _logger;

        public AuthenticateConsumer(IUserService userService, ILogger<AuthenticateConsumer> logger) 
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<AuthenticateRequest> context)
        {
            _logger.LogInformation($"Consume {nameof(AuthenticateConsumer)} data: username {context.Message.Username} password {context.Message.Password}");
            var order = await _userService.Authenticate(context.Message);
            await context.RespondAsync(order);
        }
    }
}
