﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Common;

namespace Identity.Api.Consumers.User
{
    public class GetUserByIdConsumer : IConsumer<IndexRequestResponse<string>>
    {
        private readonly IUserService _userService;
        private readonly ILogger<GetUserByIdConsumer> _logger;

        public GetUserByIdConsumer(IUserService userService, ILogger<GetUserByIdConsumer> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IndexRequestResponse<string>> context)
        {
            var order = _userService.GetUserById(context.Message.Id);
            await context.RespondAsync(order);
        }
    }
}
