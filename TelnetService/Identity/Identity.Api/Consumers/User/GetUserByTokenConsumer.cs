﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.Models.Indentity;

namespace Identity.Api.Consumers.User
{
    /// <summary>
    /// Позволяет слушать запросы по возращению информации пользователя по его токену
    /// </summary>
    public class GetUserByTokenConsumer : IConsumer<TokenRequest>
    {
        private readonly IUserService _userService;
        private readonly ILogger<GetUserByTokenConsumer> _logger;

        public GetUserByTokenConsumer(IUserService userService, ILogger<GetUserByTokenConsumer> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<TokenRequest> context)
        {
            _logger.LogInformation($"Consume: {nameof(GetUserByTokenConsumer)} | data: {context.Message.Token}");
            var order = await _userService.GetInformationUserByToken(context.Message);
            await context.RespondAsync(order);
        }
    }
}
