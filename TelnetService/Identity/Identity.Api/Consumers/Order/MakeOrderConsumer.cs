﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Requests;

namespace Identity.Api.Consumers.Order
{
    public class MakeOrderConsumer : IConsumer<MakeOrderRequest>
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<MakeOrderConsumer> _logger;

        public MakeOrderConsumer(IOrderService orderService, ILogger<MakeOrderConsumer> logger)
        {
            _orderService = orderService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<MakeOrderRequest> context)
        {
            var order = await _orderService.MakeOrder(context.Message);
            await context.RespondAsync(order);
        }
    }
}
