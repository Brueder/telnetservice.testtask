﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Common;

namespace Identity.Api.Consumers.Order
{
    /// <summary>
    /// Позволият слушать запросы по возвращению истории заказов пользователя
    /// </summary>
    public class GetOrdersByUserIdConsumer : IConsumer<IndexRequestResponse<string>>
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<GetOrdersByUserIdConsumer> _logger;

        public GetOrdersByUserIdConsumer(IOrderService orderService, ILogger<GetOrdersByUserIdConsumer> logger)
        {
            _orderService = orderService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IndexRequestResponse<string>> context)
        {
            _logger.LogInformation($"Consume {nameof(GetOrdersByUserIdConsumer)} data {context.Message.Id}");
            var order = await _orderService.GetOrdersByUserId(context.Message.Id);
            await context.RespondAsync(order);
        }
    }
}
