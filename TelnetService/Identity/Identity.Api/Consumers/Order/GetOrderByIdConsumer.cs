﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Common;

namespace Identity.Api.Consumers.Order
{
    public class GetOrderByIdConsumer : IConsumer<IndexRequestResponse<int>>
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<GetOrderByIdConsumer> _logger;

        public GetOrderByIdConsumer(IOrderService orderService, ILogger<GetOrderByIdConsumer> logger)
        {
            _orderService = orderService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IndexRequestResponse<int>> context)
        {
            var order = await _orderService.GetOrderById(context.Message.Id);
            await context.RespondAsync(order);
        }
    }
}
