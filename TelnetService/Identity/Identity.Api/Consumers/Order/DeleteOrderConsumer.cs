﻿using Identity.Core.Services;
using MassTransit;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;

namespace Identity.Api.Consumers.Order
{
    public class DeleteOrderConsumer : IConsumer<DeleteRequest<int>>
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<DeleteOrderConsumer> _logger;

        public DeleteOrderConsumer(IOrderService orderService, ILogger<DeleteOrderConsumer> logger)
        {
            _orderService = orderService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<DeleteRequest<int>> context)
        {
            _logger.LogInformation($"Consume: {nameof(DeleteOrderConsumer)} | data: {context.Message.Id}");
            await _orderService.DeleteOrder(context.Message.Id);
            await context.RespondAsync(new StandartResponse());
        }
    }
}
