﻿namespace Indentity.Api.Definitions
{
    /// <summary>
    /// Реализация интерфейса <see cref="IDefinition">
    /// </summary>
    public abstract class Definition : IDefinition
    {
        /// <summary>
        /// Конфигурация фильтров
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public virtual void ConfigureApplication(IApplicationBuilder app, IHostEnvironment env) { }
        /// <summary>
        /// Конфигурация сервисов
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public virtual void ConfigureService(IServiceCollection services, IConfiguration configuration) { }        
    }
}
