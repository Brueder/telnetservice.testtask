﻿namespace Indentity.Api.Definitions.Cors
{
    /// <summary>
    /// Конфигурация cors
    /// </summary>
    public class CorsDefinition : Definition
    {
        /// <summary>
        /// Добавляет cors в сервисы
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            var origins = configuration.GetSection("Cors")?.GetSection("Origins")?.Value?.Split(',');

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                {
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                    if (origins != null && origins.Length > 0)
                    {
                        if (origins.Contains("*"))
                        {
                            builder.AllowAnyHeader();
                            builder.AllowAnyOrigin();
                            builder.AllowAnyMethod();
                            builder.SetIsOriginAllowedToAllowWildcardSubdomains();
                            builder.SetIsOriginAllowed(host => true);
                            //builder.AllowCredentials();
                        }
                        else
                        {
                            foreach (var origin in origins)
                            {
                                builder.WithOrigins(origin);
                            }
                        }
                    }
                });
            });
        }
    }
}
