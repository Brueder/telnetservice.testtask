﻿namespace Indentity.Api.Definitions
{
    /// <summary>
    /// Обстракное представление определения микросервиса
    /// </summary>
    public interface IDefinition
    {
        /// <summary>
        /// Добавление в middlewere
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        void ConfigureService(IServiceCollection services, IConfiguration configuration);
        /// <summary>
        /// Добавление в фильтры
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        void ConfigureApplication(IApplicationBuilder app, IHostEnvironment env);
    }
}
