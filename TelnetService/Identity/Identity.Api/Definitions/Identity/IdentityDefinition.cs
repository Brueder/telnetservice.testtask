﻿using Indentity.Core.Database;
using Indentity.Core.Domain;
using Microsoft.AspNetCore.Identity;
using TelnetService.Infrastructure.Middleware;
using TelnetService.Infrastructure.Models.Indentity;

namespace Indentity.Api.Definitions.Identity
{
    public class IdentityDefinition : Definition
    {
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            //Добавление секрета
            services.Configure<ApplicationSettings>(configuration);

            services.AddAuthentication();
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
        }

        public override void ConfigureApplication(IApplicationBuilder app, IHostEnvironment env)
        {
            app.UseMiddleware<JwtMiddleware>();
        }
    }
}
