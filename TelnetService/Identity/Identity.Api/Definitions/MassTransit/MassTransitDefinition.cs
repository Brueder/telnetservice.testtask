﻿using Identity.Api.Consumers.Order;
using Identity.Api.Consumers.User;
using MassTransit;

namespace Indentity.Api.Definitions.MassTransit
{
    public class MassTransitDefinition : Definition
    {
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.AddConsumer<AuthenticateConsumer>();
                x.AddConsumer<CreateUserConsumer>();
                x.AddConsumer<GetUserByTokenConsumer>();
                x.AddConsumer<GetUserByIdConsumer>();
                x.AddConsumer<SearchUserByNameConsumer>();

                x.AddConsumer<MakeOrderConsumer>();
                x.AddConsumer<GetOrderByIdConsumer>();
                x.AddConsumer<GetOrdersByUserIdConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {

                    cfg.Host(new Uri("rabbitmq://host.docker.internal/"));
                    cfg.ReceiveEndpoint("identityQueue", e =>
                    {
                        e.PrefetchCount = 20;
                        e.UseMessageRetry(r => r.Interval(2, 100));

                        e.Consumer<GetUserByIdConsumer>(context);
                        e.Consumer<AuthenticateConsumer>(context);
                        e.Consumer<CreateUserConsumer>(context);
                        e.Consumer<GetUserByTokenConsumer>(context);
                        e.Consumer<SearchUserByNameConsumer>(context);
                    });
                    cfg.ReceiveEndpoint("orderQueue", e =>
                    {
                        e.PrefetchCount = 20;
                        e.UseMessageRetry(r => r.Interval(2, 100));

                        e.Consumer<MakeOrderConsumer>(context);
                        e.Consumer<GetOrderByIdConsumer>(context);
                        e.Consumer<GetOrdersByUserIdConsumer>(context);
                    });
                });

            });

            services.AddMassTransitHostedService();
        }
    }
}
