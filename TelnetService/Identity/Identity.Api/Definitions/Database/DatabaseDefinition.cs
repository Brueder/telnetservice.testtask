﻿using Indentity.Core.Database;
using Microsoft.EntityFrameworkCore;

namespace Indentity.Api.Definitions.Database
{
    public class DatabaseDefinition : Definition
    {
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(connectionString), ServiceLifetime.Transient);
        }
    }
}
