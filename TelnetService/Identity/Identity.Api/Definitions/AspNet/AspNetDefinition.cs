﻿namespace Indentity.Api.Definitions.AspNet
{
    public class AspNetDefinition : Definition
    {
        public override void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllers();
        }

        public override void ConfigureApplication(IApplicationBuilder app, IHostEnvironment env)
        {
            app.UseRouting();
            //app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
