﻿using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;

namespace Identity.Core.Services
{
    public interface IOrderService
    {
        Task<OrderResponse> GetOrderById(int orderid);
        Task<OrderHistoryResponse> GetOrdersByUserId(string userid);
        Task<IndexRequestResponse<int>> MakeOrder(MakeOrderRequest makeOrderRequest);
        Task DeleteOrder(int orderid);
    }
}
