﻿using Microsoft.AspNetCore.Identity;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models.Indentity;

namespace Identity.Core.Services
{
    public interface IUserService
    {
        Task<AuthenticateResponse> Authenticate(AuthenticateRequest model);
        Task<InformationUser> GetInformationUserById(string id);
        Task<IdentityResult> CreateUser(RegistrationRequest model);
        Task<InformationUser> GetInformationUserByToken(TokenRequest model);
        ApplicationUser GetUserById(string id);
        UsersResponse SearchUsersByName(string name);
        Task DeleteUser(string id);
    }
}
