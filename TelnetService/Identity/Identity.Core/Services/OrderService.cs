﻿using Indentity.Core.Database;
using MassTransit;
using Microsoft.Extensions.Logging;
using TelnetService.Infrastructure.Exceptions;
using TelnetService.Infrastructure.MassTransit.Common;
using TelnetService.Infrastructure.MassTransit.Requests;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models.Indentity;
using TelnetService.Infrastructure.Models.Product;

namespace Identity.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IBusControl _busControl;
        private readonly ILogger<OrderService> _logger;
        private readonly Uri _rabbitMqUrl = new Uri("rabbitmq://localhost/productsQueue");

        public OrderService(ApplicationDbContext applicationDbContext, IBusControl busControl, ILogger<OrderService> logger)
        {
            _applicationDbContext = applicationDbContext;
            _busControl = busControl;
            _logger = logger;
        }

        public async Task<OrderResponse> GetOrderById(int orderid)
        {
            var order = _applicationDbContext.Orders.Where(o => o.Id == orderid).FirstOrDefault();
            if(order is null)
            {
                throw new ArgumentException($"Order Id {orderid} not found");
            }

            return await GetOrderResponse(order);
        }

        public async Task<OrderHistoryResponse> GetOrdersByUserId(string userid)
        {
            var orders = _applicationDbContext.Orders.Where(o => o.User.Id == userid);
            if (orders is null)
            {
                throw new ArgumentException($"User Id {userid} not found");
            }

            //Вот здесь можно словить StackOwerFlow, мне нужо больше смотреть хорошего кода с EF
            var history = new OrderHistoryResponse { OrderResponses = new List<OrderResponse>() };
            foreach (var order in orders.ToList())
            {
                var orderResponse = await GetOrderResponse(order);
                foreach(var product in orderResponse.Products)
                {
                    history.Price += product.Price;
                    history.ProductCount++;
                }
                history.OrderResponses.Add(orderResponse);
            }

            return history;
        }

        public async Task<IndexRequestResponse<int>> MakeOrder(MakeOrderRequest makeOrderRequest)
        {
            var user = await _applicationDbContext.Users.FindAsync(makeOrderRequest.UserId);
            if (user is null)
            {
                throw new ArgumentException($"User id {makeOrderRequest.UserId} not found");
            }

            var productsResponce = await GetResponseRabbitTask<IndexRequestResponse<ICollection<int>>, ProductsResponse>
                (new IndexRequestResponse<ICollection<int>>(makeOrderRequest.ProductIds));
            if (productsResponce.Products is null || productsResponce.Products.Count() == 0)
            {
                throw new BadRequestException("Products not found");
            }

            var order = new Order
            {
                DateTime = DateTime.UtcNow,
                User = user,
            };
            //Сюда бы транзакцию
            await _applicationDbContext.Orders.AddAsync(order);
            await _applicationDbContext.SaveChangesAsync();

            List<OrderProducts> orderProducts = new List<OrderProducts>();
            foreach (var product in productsResponce.Products)
            {
                orderProducts.Add(new OrderProducts { ProductId = product.Id, Order = order, OrderId = order.Id });
            }
            
            await _applicationDbContext.OrderProducts.AddRangeAsync(orderProducts);
            await _applicationDbContext.SaveChangesAsync();

            return new IndexRequestResponse<int>(order.Id);
        }

        private async Task<OrderResponse> GetOrderResponse(Order order)
        {
            var productsOrder = _applicationDbContext.OrderProducts.Where(op => op.OrderId == order.Id);
            var products = new List<Product>();
            var ids = new IndexRequestResponse<List<int>>(new List<int>());
            foreach (var index in productsOrder)
            {
                ids.Id.Add(index.ProductId);
            }
            var productsResponce = await GetResponseRabbitTask<IndexRequestResponse<List<int>>, ProductsResponse>(ids);
            products = productsResponce.Products.ToList();

            return new OrderResponse
            {
                DateTime = order.DateTime,
                Id = order.Id,
                Products = products,
            };
        }

        private async Task<TOut> GetResponseRabbitTask<TIn, TOut>(TIn request) where TIn : class where TOut : class
        {
            var client = _busControl.CreateRequestClient<TIn>(_rabbitMqUrl);
            var response = await client.GetResponse<TOut>(request);
            return response.Message;
        }
    }
}
