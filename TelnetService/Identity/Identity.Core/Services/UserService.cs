﻿using Indentity.Core.Database;
using Indentity.Core.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TelnetService.Infrastructure.Exceptions;
using TelnetService.Infrastructure.MassTransit.Responses;
using TelnetService.Infrastructure.Models.Indentity;

namespace Identity.Core.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationSettings _appSettings;
        private readonly ApplicationDbContext _dbContext; 

        public UserService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<ApplicationSettings> appSettings, ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _dbContext = context;
        }

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);

            if (user == null || !await ValidateUser(user, model.Password))
                throw new BadRequestException("Invalid login or password!");
            var token = GenerateJwtToken(user);

            var response = new AuthenticateResponse(new InformationUser { Id = user.Id, Username = user.UserName },
                token);
            return response;
        }

        public async Task<InformationUser> GetInformationUserById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user is null)
                throw new BadRequestException("User not found!");

            var response = new InformationUser
            {
                Id = user.Id,
                Username = user.UserName
            };
            return response;
        }
        public async Task<IdentityResult> CreateUser(RegistrationRequest model)
        {
            var applicationUser = new ApplicationUser()
            {
                UserName = model.Username,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
            };

            var response = await _userManager.CreateAsync(applicationUser, model.Password);
            return response;
        }

        public async Task<InformationUser> GetInformationUserByToken(TokenRequest model)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            tokenHandler.ValidateToken(model.Token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out var validatedToken);
            var jwtToken = validatedToken as JwtSecurityToken;
            var userId = jwtToken?.Claims.First(item => item.Type == "id").Value;
            var user = await GetInformationUserById(userId);

            return user;
        }

        private async Task<bool> ValidateUser(ApplicationUser user, string password)
            => await _signInManager.UserManager.CheckPasswordAsync(user, password);

        private string GenerateJwtToken(ApplicationUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("id", user.Id)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public ApplicationUser GetUserById(string id)
        {
            return _dbContext.Users.Where(u => u.Id == id).ToList().FirstOrDefault();
        }

        public UsersResponse SearchUsersByName(string name)
        {
            var users = _dbContext.Users.Where(u => u.UserName.ToLower().Contains(name)).ToList();
            return new UsersResponse(users);
        }

        public async Task DeleteUser(string id)
        {
            var user = _dbContext.Users.Where(u => u.Id == id).FirstOrDefault();
            if(user is null)
            {
                throw new ArgumentException($"User id {id} not found");
            }

            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();
        }
    }
}

